#ifndef _MAP_H_
#define _MAP_H_

#include "filesys/file.h"

#define MAP_SIZE 128
#define MAP_OFFSET 2

typedef struct file* value_t;
typedef int key_t;

struct flist
{
  value_t content[MAP_SIZE];
};

int flist_init(struct flist** flist);
void flist_exit(struct flist** flist);

key_t flist_insert(struct flist* flist, value_t file);
void flist_remove(struct flist* flist, key_t fd);

value_t flist_find(struct flist* flist, key_t fd);

#endif
