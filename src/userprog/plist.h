#ifndef _PLIST_H_
#define _PLIST_H_

  #include "threads/synch.h"

  #define PLIST_SIZE 128
  #define PLIST_OFFSET 1

  struct process {
    int pid;
    int parent;
    int return_status;
    bool parent_alive;
    bool alive;
    struct semaphore sema;
  };

  void plist_init (void);

  struct process* plist_insert(struct process* process);
  void plist_kill(int pid);

  int plist_get_return_status(int pid);
  void plist_set_return_status(int pid, int status);

  void plist_print(void);
  int plist_wait (int pid);

#endif
