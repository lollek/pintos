#include <debug.h>
#include <stdio.h>
#include <string.h>

#include "userprog/gdt.h"      /* SEL_* constants */
#include "userprog/process.h"
#include "userprog/load.h"
#include "userprog/pagedir.h"  /* pagedir_activate etc. */
#include "userprog/tss.h"      /* tss_update */
#include "filesys/file.h"
#include "threads/init.h"
#include "threads/flags.h"     /* FLAG_* constants */
#include "threads/thread.h"
#include "threads/vaddr.h"     /* PHYS_BASE */
#include "threads/interrupt.h" /* if_ */

/* Headers not yet used that you may need for various reasons. */
#include "threads/synch.h"
#include "threads/malloc.h"
#include "lib/kernel/list.h"

#include "userprog/flist.h"
#include "userprog/plist.h"


/* This function is called at boot time (threads/init.c) to initialize
 * the process subsystem. */
void
process_init (void)
{
  plist_init();
}

void
process_exit (int status)
{
  plist_set_return_status(thread_current ()->tid, status);
  thread_exit ();
}

void
process_print_list (void)
{
  plist_print();
}


struct parameters_to_start_process
{
  char* command_line;
  struct semaphore done;
  bool success;
  int parent_pid;
};

static void
start_process (struct parameters_to_start_process* parameters) NO_RETURN;

/* Starts a new proccess by creating a new thread to run it. The
   process is loaded from the file specified in the COMMAND_LINE and
   started with the arguments on the COMMAND_LINE. The new thread may
   be scheduled (and may even exit) before process_execute() returns.
   Returns the new process's thread id, or TID_ERROR if the thread
   cannot be created. */
int
process_execute (const char *command_line) 
{
  char debug_name[64];
  int command_line_size = strlen(command_line) + 1;
  tid_t thread_id = -1;
  int  process_id = -1;

  /* LOCAL variable will cease existence when function return! */
  struct parameters_to_start_process arguments;
  arguments.parent_pid = thread_current ()->tid;

  debug("%s#%d: process_execute(\"%s\") ENTERED\n",
        thread_current()->name,
        thread_current()->tid,
        command_line);

  /* COPY command line out of parent process memory */
  arguments.command_line = malloc(command_line_size);
  if (arguments.command_line == NULL)
    return -1;
  strlcpy(arguments.command_line, command_line, command_line_size);

  /* Init the semaphore */
  sema_init(&arguments.done, 0);

  strlcpy_first_word (debug_name, command_line, 64);
  
  /* SCHEDULES function `start_process' to run (LATER) */
  thread_id = thread_create (debug_name, PRI_DEFAULT,
                             (thread_func*)start_process, &arguments);

  if (thread_id != -1)
  {
    sema_down(&arguments.done);
    process_id = arguments.success ? thread_id : -1;
  }
  free(arguments.command_line);

  debug("%s#%d: process_execute(\"%s\") RETURNS %d\n",
        thread_current()->name,
        thread_current()->tid,
        command_line, process_id);

  /* MUST be -1 if `load' in `start_process' return false */
  return process_id;
}

static void
parse_command_line (const char* command_line, int* line_size, int* argc)
{
  int i;
  *line_size = 0;
  *argc = 0;

  while (*command_line == ' ')
    ++command_line;

  for (i = 0; command_line[i] != '\0'; ++i)
  {
    ++*line_size;
    if (command_line[i] == ' ')
    {
      ++*argc;
      while (command_line[i] == ' ' && command_line[i + 1] == ' ')
        ++i;
    }
  }

  if (i > 0 && command_line[i - 1] != ' ')
    ++*argc;
  ++*line_size; /* null-terminator */
}

static void
build_args (char* dst, const char* src, int argc, char** argv)
{
  char* start_ptr = dst;
  int arg_counter = 0;
  int i;
  int j;

  while (*src == ' ')
    ++src;

  for (i = 0, j = 0; src[i] != '\0'; ++i, ++j)
  {
    if (src[i] == ' ')
    {
      argv[arg_counter++] = start_ptr;
      dst[j] = '\0';
      while (src[i] == ' ' && src[i + 1] == ' ')
        ++i;
      start_ptr = &dst[j + 1];
    }
    else
      dst[j] = src[i];
  }
  dst[j] = '\0';
  if (arg_counter < argc)
    argv[arg_counter] = start_ptr;
}

static void*
setup_main_stack (const char* command_line, void* stack_top)
{
  struct main_args
  {
    void (*ret)(void);
    int argc;
    char** argv;
  }* esp;

  int argc;
  int total_size;
  int line_size;
  char* cmd_line_on_stack;

  /* Count argc and command line size */
  parse_command_line (command_line, &line_size, &argc);

  /* Align line size to 4 bytes */
  line_size = line_size % 4 ? line_size + 4 - line_size % 4 : line_size;

  /* Set total stack use size
     NULL-ptr(4) + argc(4) + argv(4 + argc*4 + * 4) + line_size */
  total_size = 4 + 4 + 4 + argc*4 + 4 + line_size;

  esp = (struct main_args*)((unsigned)stack_top - total_size);
  esp->ret = NULL;
  esp->argc = argc;
  esp->argv = (char**)((unsigned)esp + 4 + 4 + 4);

  /* Copy arguments, build argv, and replace spaces with nulls */
  cmd_line_on_stack = (char*)esp->argv + esp->argc*4 + 4;
  build_args (cmd_line_on_stack, command_line, esp->argc, esp->argv);

  return esp; /* the new stack top */
}

/* A thread function that loads a user process and starts it
   running. */
static void
start_process (struct parameters_to_start_process* parameters)
{
  /* The last argument passed to thread_create is received here... */
  struct intr_frame if_;

  char file_name[64];
  strlcpy_first_word (file_name, parameters->command_line, 64);
  
  debug("%s#%d: start_process(\"%s\") ENTERED\n",
        thread_current()->name,
        thread_current()->tid,
        parameters->command_line);
  
  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;

  parameters->success = load (file_name, &if_.eip, &if_.esp);

  debug("%s#%d: start_process(...): load returned %d\n",
        thread_current()->name,
        thread_current()->tid,
        parameters->success);
  
  if (parameters->success)
  {
    struct process process = {
      .pid = thread_current ()->tid,
      .parent = parameters->parent_pid,
      .return_status = -1,
      .parent_alive = true,
      .alive = true
    };

    if (plist_insert (&process) == NULL)
      parameters->success = false;
  }

  if (parameters->success)
    if_.esp = setup_main_stack (parameters->command_line, if_.esp);

  debug ("%s#%d: start_process(\"%s\") DONE\n",
        thread_current ()->name,
        thread_current ()->tid,
        parameters->command_line);

  sema_up (&parameters->done);


  /* If load fail, quit. Load may fail for several reasons.
     Some simple examples:
     - File does not exist
     - File does not contain a valid program
     - Not enough memory
  */
  if (!parameters->success)
    thread_exit ();

  /* Start the user process by simulating a return from an interrupt,
     implemented by intr_exit (in threads/intr-stubs.S). Because
     intr_exit takes all of its arguments on the stack in the form of
     a `struct intr_frame', we just point the stack pointer (%esp) to
     our stack frame and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();
}

/* Wait for process `child_id' to die and then return its exit
   status. If it was terminated by the kernel (i.e. killed due to an
   exception), return -1. If `child_id' is invalid or if it was not a
   child of the calling process, or if process_wait() has already been
   successfully called for the given `child_id', return -1
   immediately, without waiting.

   This function will be implemented last, after a communication
   mechanism between parent and child is established. */
int
process_wait (int child_id) 
{
  struct thread *cur = thread_current ();
  int status = -1;

  debug("%s#%d: process_wait(%d) ENTERED\n",
        cur->name, cur->tid, child_id);

  status = plist_wait (child_id);

  debug("%s#%d: process_wait(%d) RETURNS %d\n",
        cur->name, cur->tid, child_id, status);
  
  return status;
}

/* Free the current process's resources. This function is called
   automatically from thread_exit() to make sure cleanup of any
   process resources is always done. That is correct behaviour. But
   know that thread_exit() is called at many places inside the kernel,
   mostly in case of some unrecoverable error in a thread.

   In such case it may happen that some data is not yet available, or
   initialized. You must make sure that nay data needed IS available
   or initialized to something sane, or else that any such situation
   is detected.
*/
  
void
process_cleanup (void)
{
  struct thread  *cur = thread_current ();
  uint32_t       *pd  = cur->pagedir;
  int status = -1;

  debug("%s#%d: process_cleanup() ENTERED\n", cur->name, cur->tid);

  status = plist_get_return_status(cur->tid);
  plist_kill (cur->tid);

  
  /* Later tests DEPEND on this output to work correct. You will have
   * to find the actual exit status in your process list. It is
   * important to do this printf BEFORE you tell the parent process
   * that you exit.  (Since the parent may be the main() function,
   * that may sometimes poweroff as soon as process_wait() returns,
   * possibly before the prontf is completed.)
   */
  printf("%s: exit(%d)\n", thread_name(), status);

  
  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  if (pd != NULL) 
    {
      /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
      cur->pagedir = NULL;
      pagedir_activate (NULL);
      pagedir_destroy (pd);
    }  
  debug("%s#%d: process_cleanup() DONE with status %d\n",
        cur->name, cur->tid, status);
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

