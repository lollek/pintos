#include <stdio.h>
#include <syscall-nr.h>
#include <string.h>

#include "userprog/plist.h"
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "devices/timer.h"

#include "userprog/syscall.h"

/* header files you probably need, they are not used yet */
#include "threads/vaddr.h"
#include "threads/init.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include "devices/input.h"

static void syscall_handler (struct intr_frame *);

static bool verify_fix_length(const void* start, int length)
{
  if (length <= 0 || start <= NULL || start + length >= PHYS_BASE)
    return false;

  void *end_page = pg_round_down (start + length -1);

  void* page;
  for (page = pg_round_down (start); page <= end_page; page += PGSIZE)
    if (pagedir_get_page (thread_current ()->pagedir, page) == NULL)
      return false;

  return true;
}

static bool verify_variable_length(const char* start)
{
  if ((void*)start <= NULL || (void*)start >= PHYS_BASE)
    return false;
  if (pagedir_get_page (thread_current ()->pagedir, start) == NULL)
    return false;

  char *table_start = pg_round_down (start);
  while (*start != '\0')
  {
    ++start;
    if ((void*)start >= PHYS_BASE)
      return false;
    if (start - table_start >= PGSIZE)
    {
      table_start = pg_round_down(start);
      if (pagedir_get_page(thread_current ()->pagedir, start) == NULL)
        return false;
    }
  }
  return true;
}


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
sys_halt (void)
{
  power_off ();
}

static void
sys_exit (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  int32_t status = ((int32_t*)f->esp)[1];

  process_exit (status);
}

static void
sys_wait (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  tid_t child_tid = ((int32_t*)f->esp)[1];

  f->eax = process_wait (child_tid);
}

static void
sys_exec (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  const char* cmd = (char*)((uint32_t*)f->esp)[1];

  if (!verify_variable_length(cmd))
    thread_exit();

  f->eax = process_execute(cmd);
}

static void
sys_sleep (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  int32_t time = ((int32_t*)f->esp)[1];

  timer_msleep(time);
}

static void
sys_plist (struct intr_frame *f)
{
  (void)f;

  process_print_list();
}

static void
sys_read (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 16))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];
  uint8_t* buf = (uint8_t*)((uint32_t*)f->esp)[2];
  uint32_t length = ((uint32_t*)f->esp)[3];

  if (length == 0)
  {
    f->eax = 0;
    return;
  }

  if (!verify_fix_length (buf, length))
    thread_exit();


  if ( fd == STDIN_FILENO )
  {
    for ( f->eax = 0; f->eax < length; ++(f->eax) )
    {
      uint8_t c = input_getc ();
      buf[f->eax] = c == '\r' ? '\n' : c;
      putchar (buf[f->eax]);
    }
    return;
  }

  struct file* file = flist_find(thread_current ()->flist, fd);
  if (file != NULL)
  {
    f->eax = file_read(file, buf, length);
    return;
  }

  f->eax = -1;
}

static void
sys_write (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 16))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];
  const char* buf = (char*)((uint32_t*)f->esp)[2];
  uint32_t length = ((uint32_t*)f->esp)[3];

  if (length == 0)
  {
    f->eax = 0;
    return;
  }

  if (!verify_fix_length (buf, length))
    thread_exit();

  if ( fd == STDOUT_FILENO )
  {
    putbuf (buf, length);
    f->eax = length;
    return;
  }

  struct file* file = flist_find(thread_current ()->flist, fd);
  if (file != NULL)
  {
    f->eax = file_write(file, buf, length);
    return;
  }

  if (file == NULL)
    debug("!!! FILE == NULL(fd: %d)\n", fd);

  f->eax = -1;
}

static void
sys_create (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 12))
    thread_exit ();

  const char* name = (char*)((uint32_t*)f->esp)[1];
  uint32_t initial_size = ((uint32_t*)f->esp)[2];

  if (!verify_variable_length(name))
    thread_exit();

  f->eax = filesys_create (name, initial_size);
}

static void
sys_remove (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  const char* name = (char*)((uint32_t*)f->esp)[1];

  if (!verify_variable_length(name))
    thread_exit();

  f->eax = filesys_remove (name);
}

static void
sys_open (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  const char* name = (char*)((uint32_t*)f->esp)[1];

  if (!verify_variable_length(name))
    thread_exit();

  struct file* file = filesys_open (name);
  if (file == NULL)
  {
    f->eax = -1;
    return;
  }
  
  f->eax = flist_insert (thread_current ()->flist, file);
  if ((int32_t)f->eax == -1)
    filesys_close (file);
}

static void
sys_close (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];

  filesys_close (flist_find (thread_current ()->flist, fd));

  flist_remove (thread_current ()->flist, fd);
}

static void
sys_seek (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 12))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];
  uint32_t position = ((uint32_t*)f->esp)[2];

  struct file* file = flist_find (thread_current ()->flist, fd);
  if (file == NULL)
    return;

  if ((off_t)position < 0 || (off_t)position >= file_length (file))
    return;

  file_seek (file, (off_t)position);
}

static void
sys_tell (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];

  struct file* file = flist_find (thread_current ()->flist, fd);
  if (file == NULL)
  {
    f->eax = -1;
    return;
  }

  f->eax = file_tell (file);
}

static void
sys_filesize (struct intr_frame *f)
{
  if (!verify_fix_length(f->esp, 8))
    thread_exit ();

  int32_t fd = ((int32_t*)f->esp)[1];

  struct file* file = flist_find (thread_current ()->flist, fd);
  if (file == NULL)
  {
    f->eax = -1;
    return;
  }

  f->eax = file_length (file);
}

static void
syscall_handler (struct intr_frame *f)
{
  uint32_t* esp = (uint32_t*)f->esp;

  if (!verify_fix_length (f->esp, 4))
    thread_exit();

  uint32_t syscall_number = esp[0];

  switch ( syscall_number )
  {
    case SYS_HALT: sys_halt (); break;
    case SYS_EXIT: sys_exit (f); break;
    case SYS_EXEC: sys_exec (f); break;
    case SYS_WAIT: sys_wait (f); break;
    case SYS_SLEEP: sys_sleep (f); break;
    case SYS_PLIST: sys_plist (f); break;
    case SYS_READ: sys_read (f); break;
    case SYS_WRITE: sys_write (f); break;
    case SYS_CREATE: sys_create (f); break;
    case SYS_REMOVE: sys_remove (f); break;
    case SYS_OPEN: sys_open (f); break;
    case SYS_CLOSE: sys_close (f); break;
    case SYS_TELL: sys_tell(f); break;
    case SYS_SEEK: sys_seek(f); break;
    case SYS_FILESIZE: sys_filesize(f); break;

    default:
    {
      printf ("Executed an unknown system call!\n");
      thread_exit ();
    }
  }
}
