#include <stddef.h>

#include "filesys/filesys.h"
#include "threads/malloc.h"

#include "flist.h"

int
flist_init (struct flist** flist)
{
  *flist = calloc (1, sizeof(struct flist));

  ASSERT (*flist != NULL);
  return *flist != NULL;
}

void
flist_exit (struct flist** flist)
{
  int i;
  for (i = 0; i < MAP_SIZE; ++i)
    filesys_close ((*flist)->content[i]);

  free (*flist);
  *flist = NULL;
}
  
key_t
flist_insert (struct flist* flist, value_t file)
{
  int i;
  for (i = 0; i < MAP_SIZE; ++i)
  	if (flist->content[i] == NULL) 
  	{
  	  flist->content[i] = file;
  	  return i + MAP_OFFSET;
  	}

  return -1;
}

void
flist_remove (struct flist* flist, key_t fd)
{
  if (fd < MAP_OFFSET || fd > MAP_SIZE + MAP_OFFSET)
  	return;
  flist->content[fd - MAP_OFFSET] = NULL;
}


value_t
flist_find (struct flist* flist, key_t fd)
{
  if (fd < MAP_OFFSET || fd > MAP_SIZE + MAP_OFFSET)
  	return NULL;
  return flist->content[fd - MAP_OFFSET];
}
