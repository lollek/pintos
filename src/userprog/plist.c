#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "threads/malloc.h"

#include "plist.h"

static struct process* process_list[PLIST_SIZE];

static struct lock plist_lock;

void
plist_init (void)
{
  lock_init(&plist_lock);
}

struct process*
plist_insert (struct process* process)
{
  int i;
  struct process* return_value = NULL;

  lock_acquire(&plist_lock);

  for (i = 0; i < PLIST_SIZE; ++i)
  {
    if (process_list[i] == NULL)
    {
      process_list[i] = malloc(sizeof *process);
      memcpy(process_list[i], process, sizeof *process);
      sema_init(&process_list[i]->sema, 0);
      return_value = process_list[i];
      break;
    }
  }

  lock_release(&plist_lock);
  return return_value;
}

static void
plist_remove (int i)
{
  free(process_list[i]);
  process_list[i] = NULL;
}

void
plist_kill (int pid)
{
  int i;

  lock_acquire(&plist_lock);

  for (i = 0; i < PLIST_SIZE; ++i)
  {
    if (process_list[i] == NULL)
      continue;

    if (process_list[i]->pid == pid)
    {
      if (process_list[i]->parent_alive == false)
        plist_remove(i);
      else
      {
        process_list[i]->alive = false;
        sema_up(&process_list[i]->sema);
      }
    }

    else if (process_list[i]->parent == pid)
    {
      if (process_list[i]->alive == false)
        plist_remove(i);
      else
        process_list[i]->parent_alive = false;
    }
  }

  lock_release(&plist_lock);
}


static struct process*
plist_find (int pid)
{
  int i;
  for (i = 0; i < PLIST_SIZE; ++i)
    if (process_list[i] != NULL && process_list[i]->pid == pid)
      return process_list[i];
  return NULL;
}

int
plist_get_return_status(int pid)
{
  int return_value = -1;
  lock_acquire(&plist_lock);

  struct process* proc = plist_find(pid);
  if (proc != NULL)
    return_value = proc->return_status;

  lock_release(&plist_lock);
  return return_value;
}

void
plist_set_return_status(int pid, int status)
{
  lock_acquire(&plist_lock);

  struct process* proc = plist_find(pid);
  if (proc != NULL)
    proc->return_status = status;

  lock_release(&plist_lock);
}

void
plist_print (void)
{
  int i;

  lock_acquire(&plist_lock);

  printf ("PID\tParent\tAlive\tParentAlive\tReturnValue\n");
  for (i = 0; i < PLIST_SIZE; ++i)
    if (process_list[i] != NULL)
      printf ("%3d\t%6d\t%5d\t%11d\t%11d\n", process_list[i]->pid,
          process_list[i]->parent, process_list[i]->alive,
          process_list[i]->parent_alive, process_list[i]->return_status);

  lock_release(&plist_lock);
}

int
plist_wait (int pid)
{
  int i;

  lock_acquire(&plist_lock);

  for (i = 0; i < PLIST_SIZE; ++i)
    if (process_list[i] != NULL && process_list[i]->pid == pid)
      break;

  lock_release(&plist_lock);

  if (i == PLIST_SIZE)
    return -1;

  sema_down (&process_list[i]->sema);

  lock_acquire(&plist_lock);

  int retval = process_list[i]->return_status;
  plist_remove(i);

  lock_release(&plist_lock);

  return retval;
}
